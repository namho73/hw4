﻿using System;
using System.Collections.Generic;
using System.Text;

namespace hw4
{
    public class MovieViewModel
    {
        public string Name { get; set; }
        public string Genre { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string Url { get; set; }
    }
}
