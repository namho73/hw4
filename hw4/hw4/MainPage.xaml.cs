﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace hw4
{
	public partial class MainPage : ContentPage
	{
        public ObservableCollection<MovieViewModel> Movies { get; set; }
        public MainPage()
		{
			InitializeComponent();
            Movies = new ObservableCollection<MovieViewModel>();
            Movies.Add(new MovieViewModel { Name = "Der Untergang",
                                            Genre = "War, Drama",
                                            Description = "In April of 1945, Germany stands at the brink of defeat with the Soviet Armies closing in from the west and south. In Berlin, " +
                                            "capital of the Third Reich, Adolf Hitler proclaims that Germany will still achieve victory and orders his Generals and advisers to fight to " +
                                            "the last man. \"Downfall\" explores these final days of the Reich, where senior German leaders (such as Himmler and Goring) began defecting from " +
                                            "their beloved Fuhrer, in an effort to save their own lives, while still others (Joseph Goebbels) pledge to die with Hitler. Hitler, himself, " +
                                            "degenerates into a paranoid shell of a man, full of optimism one moment and suicidal depression the next. When the end finally does comes, and " +
                                            "Hitler lies dead by his own hand, what is left of his military must find a way to end the killing that is the Battle of Berlin, and lay down their " +
                                            "arms in surrender.",
                                            Image = "derUntergang.png",
                                            Url = "https://www.imdb.com/title/tt0363163/" });
            Movies.Add(new MovieViewModel { Name = "Unsere Mütter, unsere Väter",
                                            Genre = "War, Drama",
                                            Description = "Berlin, 1941. Five friends eager to become heroes in an adventure that will change the face of Europe - and that will forever change " +
                                            "them as well. Level-headed, highly decorated officer Wilhelm is off to the eastern front with his younger brother Friedhelm, a sensitive dreamer more " +
                                            "interested in literature than warfare. Deeply in love with Wilhelm is Charlotte, a young nurse who looks forward to serving in the Wehrmacht, also on " +
                                            "the eastern front. While Greta is a talented singer who longs to become another Marlene Dietrich, her Jewish boyfriend Viktor still cannot convince his " +
                                            "parents to leave Germany... Valor and courage come to the fore, but also betrayal - of values, beliefs, humanity. Friedhelm turns into a soulless killing" +
                                            " machine... Wilhelm deserts his troops and is court-martialed... Charlotte's Nazi ideology crumbles when she betrays a Jewish nurse helping the German army... " +
                                            "Greta obtains papers for Viktor's escape by selling herself to an SS colonel. They and millions of others wanted to be heroes; but none of them could imagine " +
                                            "what the war would ultimately do to them and to the rest of the world.",
                                            Image = "theirMotherTheirFather.png",
                                            Url = "https://www.imdb.com/title/tt1883092/" });
            
            Movies.Add(new MovieViewModel { Name = "Stalingrad",
                                            Genre = "War",
                                            Description = "In August 1942, German soldiers enjoy leave in Cervo, Liguria, Italy after fighting in North Africa. An assembly is then held and several of the " +
                                            "men are awarded medals, including Unteroffizier Manfred \"Rollo\" Rohleder" +
                                            " and Obergefreiter Fritz Reiser, who are both introduced to Leutnant Hans von Witzland, their new platoon commander. " +
                                            "The unit is promptly sent to the Eastern Front to participate in the Battle of Stalingrad.",
                                            Image = "stalingrad.png",
                                            Url = "https://www.imdb.com/title/tt0108211/" });
            Movies.Add(new MovieViewModel { Name = "Suite Française",
                                            Genre = "Drama",
                                            Description = "In German-occupied France, Lucile Angellier and her domineering mother-in-law Madame Angellier await news of her husband, who was serving in the French Army. " +
                                            "While visiting tenants, Lucile and Madame Angellier escape an air raid by German Ju-87 stuka bombers. Following the French surrender, a regiment of German soldiers arrives, and " +
                                            "promptly moves into the homes of the villagers. \n\n The Wehrmacht Oberleutnant Bruno von Falk is billeted at the Angelliers' household. Lucile tries to ignore " +
                                            "Bruno but is charmed by his kindly demeanor and his piano music. Lucile later learns that her husband Gaston's unit has been imprisoned at a German labor camp. " +
                                            "Elsewhere, the farmer Benoit and his wife Madeleine chafe under the German officer Kurt Bonnet, who harasses Madeleine.Benoit, who was denied the chance to fight " +
                                            "because of his leg wound, hides a rifle.As an act of resistance, he steals the German soldiers' clothes while they are bathing.",
                                            Image = "suiteFrancaise.png",
                                            Url = "https://www.imdb.com/title/tt0900387/" });

            Movies.Add(new MovieViewModel
            {
                Name = "My Honor Was Loyalty",
                Genre = "Indie, War, Drama",
                Description = "Untersharführer Ludwig Herckel, is a devoted and patriotic soldier of the elite 1st SS Panzer Division Leibstandarte SS Adolf Hitler. " +
                "As the action begins, we follow a small group of experienced infantry soldiers in combat against Soviet forces. An explosion knocks Herschel unconscious and " +
                "he wanders the woods in a daze. He is met by another soldier who happens to be from his same town back in Germany. This solder tells Herschel, in confidence, " +
                "he worries about his wife as she is Jewish. When Herschel goes on leave back to Germany, he looks up the wife and finds the Gestapo attempting to arrest her. " +
                "When she runs, a Gestapo officer shoots and kills her. Herschel is troubled by this act, but returns to his outfit anyway. Now posted to the western front, " +
                "Herschel witnesses the decline of the army group, loss of comrades in arms, and multiple acts of war crimes in the handling of POW's for both sides.",
                Image = "myHonorWasMyLoyalty.png",
                Url = "https://www.imdb.com/title/tt4544696/"
            });

            lstView.ItemsSource = Movies;
        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var listView = (ListView)sender;
            MovieViewModel itemTapped = (MovieViewModel)listView.SelectedItem;
            var uri = new Uri(itemTapped.Url);
            Device.OpenUri(uri);
        }

        

        void Handle_MoreButton(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var movie = (MovieViewModel)menuItem.CommandParameter;
            Navigation.PushAsync(new MoreInfoPage(movie));
        }

        /*
        void(object sender, EventArgs e)
        {
            var item = (MenuItem)sender; ;
            MovieViewModel listitem = (from itm in Movies
                             where itm.Name == item.CommandParameter.ToString()
                             select itm)
                            .FirstOrDefault<MovieViewModel>();
            Movies.Remove(listitem);
        }
        */

        void Handle_DeleteButton(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var movie = (MovieViewModel)menuItem.CommandParameter;
            
            Movies.Remove(movie);
        }

        
    }
}
